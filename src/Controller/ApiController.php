<?php

namespace App\Controller;

use App\Entity\BrutForceEntity;
use App\Entity\Token;
use App\Entity\Utilisateur;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api', name: 'app_api')]
class ApiController extends AbstractController
{

    public function __construct(
        private SerializerInterface $serializer,
        private EntityManagerInterface $entityManager,
        private UserPasswordHasherInterface $passwordEncoder,
    ) {
    }


    #[Route('/', name: '_index')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ApiController.php',
        ]);
    }

    #[Route('/token', name: '_token', methods: ["POST"], stateless: true)]
    public function createTokenRequest(Request $request): JsonResponse|Response
    {
        try {
            if (empty($request->getContent())) throw new Exception("Identifiants inexistant (paire login / mot de passe)", 404);
            else {
                $content = json_decode(json_decode($this->serializer->serialize($request->getContent(), "json"), true));
                if (empty($content->from)) $content->from = $request->server->get("REQUEST_URI");
                $users = $this->entityManager->getRepository(Utilisateur::class)->findBy(["login" => $content->login]);
                $id = null;
                foreach ($users as $u) {
                    if ($this->entityManager->getRepository(Token::class)->verifyPassword($content->password, $u->getPassword())) {
                        $id = $u->getId();
                        break;
                    }
                }
                if (empty($id)) $this->entityManager->getRepository(BrutForceEntity::class)->brutForche($content->login, $content->password, $request->getClientIps());
                $user = $this->entityManager->getRepository(Utilisateur::class)->find($id);
                $jwt = new Token();
                $jwt->JWT($user->getUid());
                $jwt->setUtilisateur($user);
                
                $this->entityManager->persist($jwt);
                $this->entityManager->flush();

                return new Response(json_encode([
                    "accessToken" => $jwt->getAccessToken(),
                    "accessTokenExpiresAt" => \date(DateTime::ISO8601_EXPANDED, $jwt->getAccessTokenExpiresAt()),
                    "refreshToken" => $jwt->getRefreshToken(),
                    "refreshTokenExpiresAt" => \date(DateTime::ISO8601_EXPANDED, $jwt->getRefreshTokenExpiresAt())
                ]), 201, ["content-type" => "application/json"]);
            }
        } catch (\Exception $e) {
            return $this->json([$e->getMessage()], $e->getCode());
        }
    }

    #[Route('/validation/{accessToken}', name: '_validation', methods: ["GET"], stateless: true)]
    public function validationToken(string $accessToken, Request $request): JsonResponse|Response {
        $jwt = $this->entityManager->getRepository(Token::class)->findOneBy(["accessToken" => $accessToken]);
        try {
            if (empty($jwt)) { throw new \UnexpectedValueException("Invalide"); }
            $token = $jwt->decodeJWT($accessToken);
        } catch (\Throwable $th) {
            return $this->json("Token non trouvé / Invalide", 404);
        }

        return new Response(json_encode([
            "accessToken" => $jwt->getAccessToken(),
            "accessTokenExpiresAt" => \date(DateTime::ISO8601_EXPANDED, $jwt->getAccessTokenExpiresAt()),
        ]), 200, ["content-type" => "application/json"]);
    }

    #[Route('/refresh-token/{refreshToken}/token', name: '_refresh-token', methods: ["POST"], stateless: true)]
    public function refreshToken(string $refreshToken) {
        $jwt = $this->entityManager->getRepository(Token::class)->findOneBy(["refreshToken" => $refreshToken]);
        try {
            if (empty($jwt)) throw new \UnexpectedValueException("Invalide");
            $refreshtoken = $jwt->decodeJWT($refreshToken);
        } catch (\Throwable $th) {
            return $this->json("Token invalide ou inexistant", 404);
        }

        $user = $refreshtoken["sub"];

        $jwt->JWT($user);

        $this->entityManager->persist($jwt);
        $this->entityManager->flush();

        return new Response(json_encode([
            "accessToken" => $jwt->getAccessToken(),
            "accessTokenExpiresAt" => \date(DateTime::ISO8601_EXPANDED, $jwt->getAccessTokenExpiresAt()),
            "refreshToken" => $jwt->getRefreshToken(),
            "refreshTokenExpiresAt" => \date(DateTime::ISO8601_EXPANDED, $jwt->getRefreshTokenExpiresAt())
        ]), 201, ["content-type" => "application/json"]);
    }

}
