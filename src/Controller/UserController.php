<?php

namespace App\Controller;

use App\Entity\Token;
use App\Entity\Utilisateur;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Factory\UuidFactory;

#[Route('/api', name: 'app_account', stateless: true)]
class UserController extends AbstractController
{

    public function __construct(
        private SerializerInterface $serializer,
        private EntityManagerInterface $entityManager,
        private UuidFactory $uuidFactory,
        private UserPasswordHasherInterface $passwordEncoder,
        private Security $security,
    ) {
    }


    #[Route('/', name: '_index')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserController.php',
        ]);
    }

    #[Route('/account', name: '_create_user', methods: ['POST'])]
    public function createUser(Request $request): JsonResponse
    {
        try {
            $data = json_decode($request->getContent(), true);
            $login = $data['login'];
            $password = $data['password'];
            $status = empty($data["status"]) ? "open" : $data["status"];
            $roles = [];
            if (empty($data["roles"]) || \count($data["roles"]) == 0) {
                $roles = array('ROLE_USER');
            }
            elseif (!is_array($data["roles"])) throw new \Exception("Paramètres de connection invalide", 422);
            else {
                $roles = $data["roles"];
                if (in_array("ROLE_ADMIN", $roles)) {
                    if (!$request->headers->has("Authentication")) throw new \Exception("Il est nécessaire d'être authentifié", 401);
                    else {
                        $authentication = explode(' ', $request->headers->get("Authentication"));
                        if ("Bearer" != $authentication[0]) throw new \Exception("Paramètres de connection invalide admin token manquant et / ou incorrect", 422);
                        else {
                            $jwt = $this->entityManager->getRepository(Token::class)->findOneBy(["accessToken" => $authentication[1]]);
                            if (empty($jwt)) throw new \UnexpectedValueException("Paramètres de connection invalide admin token manquant et / ou incorrect", 422);
                            $token = $jwt->decodeJWT($authentication[1]);
                            $entity = $this->entityManager->getRepository(Utilisateur::class)->findOneBy(["uid" => $token["sub"]]);
                            if (!in_array("ROLE_ADMIN", $entity->getRoles())) throw new \Exception("Il est nécessaire de disposer d'un compte admin pour créer un compte admin", 403);
                        }
                    }
                }
                
            }
            $user = \count($this->entityManager->getRepository(Utilisateur::class)->findAll());
            
            if ($user == 0) $roles[] = "ROLE_ADMIN";
            
            $newUser = new Utilisateur();
            $newUser->setLogin($login);
            $newUser->setUid($this->uuidFactory->create());
            $newUser->setPassword($this->entityManager->getRepository(Token::class)->passwordHashed($password));
            $newUser->setStatus($status);
            $newUser->setRoles($roles);
            $newUser->setCreateAt(new DateTimeImmutable());
            $newUser->setUpdatedAt(new DateTimeImmutable());

            $this->entityManager->persist($newUser);
            $this->entityManager->flush();

            return $this->json([
                "uid" => $newUser->getUid(),
                "login" => $newUser->getLogin(),
                "roles" => $newUser->getRoles(),
                "createdAt" => $newUser->getCreateAt(),
                "updatedAt" => $newUser->getUpdatedAt(),
            ], 201);

        } catch (\Exception $e) {
            return $this->json([$e->getMessage()], $e->getCode());
        }
    }

    #[Route('/account/{uid}', name: '_get_user', methods: ['GET', 'HEAD'])]
    public function getUtilisateur(string $uid, Request $request): JsonResponse
    {
        try {
            $user = null;
            if ($uid == "me") {
                if ($request->headers->has("Authentication")) {
                    $authentication = explode(' ', $request->headers->get("Authentication"));
                    if ("Bearer" != $authentication[0]) throw new \Exception("Il est nécessaire d'être authentifié", 401);
                    else {
                        $jwt = $this->entityManager->getRepository(Token::class)->findOneBy(["accessToken" => $authentication[1]]);
                        if (empty($jwt)) throw new \UnexpectedValueException("Il est nécessaire d'être authentifié", 401);
                        $token = $jwt->decodeJWT($authentication[1]);
                        $user = $this->entityManager->getRepository(Utilisateur::class)->findOneBy(["uuid" => $token["sub"]]);

                        // if (!in_array("ROLE_ADMIN", $entity->getRoles())) throw new \Exception("Il est nécessaire de disposer d'un compte admin pour créer un compte admin", 403);
                    }
                }
            }
            elseif ($uid != "me" && $request->headers->has("Authentication")) {
                $authentication = explode(' ', $request->headers->get("Authentication"));
                if ("Bearer" != $authentication[0]) throw new \Exception("Il est nécessaire d'être authentifié", 401);
                else {
                    $jwt = $this->entityManager->getRepository(Token::class)->findOneBy(["accessToken" => $authentication[1]]);
                    if (empty($jwt)) throw new \UnexpectedValueException("Il est nécessaire d'être authentifié", 401);
                    $token = $jwt->decodeJWT($authentication[1]);
                    $useredit = $this->entityManager->getRepository(Utilisateur::class)->findOneBy(["uuid" => $token["sub"]]);
                    if (!in_array("ROLE_ADMIN", $useredit->getRoles())) throw new \Exception("Il est nécessaire d'être admin ou d'être le proriétaire du compte", 403);
                    else{
                        unset($token, $jwt, $useredit);
                        $user = $this->entityManager->getRepository(Utilisateur::class)->findOneBy(['uuid' => $uid]);
                    }
                }            
            }
            else $user = $this->entityManager->getRepository(Utilisateur::class)->findOneBy(['uid' => $uid]);
            

            if (!$user) return $this->json(['erreur' => 'Aucun utilisateur trouvé avec l\'UID donné'], 404);
            
            
            $userArray = $this->serializer->serialize($user, "json", ['attributes' => ['id', 'uuid', 'login', 'roles', 'status', 'createAt', 'updatedAt']]);
            return $this->json(json_decode($userArray), 200);
        } catch (\Exception $e) {
            return $this->json([$e->getMessage()], $e->getCode());
        }
    }

    #[Route('/account/{uid}', name: '_update_user', methods: ['PUT'])]
    public function updateUser(Request $request, string $uid): JsonResponse
    {
        try {
            if ($uid == "me") {
                if ($request->headers->has("Authentication")) {
                    $authentication = explode(' ', $request->headers->get("Authentication"));
                    if ("Bearer" != $authentication[0]) throw new \Exception("Il est nécessaire d'être authentifié", 401);
                    else {
                        $jwt = $this->entityManager->getRepository(Token::class)->findOneBy(["accessToken" => $authentication[1]]);
                        if (empty($jwt)) throw new \UnexpectedValueException("Il est nécessaire d'être authentifié", 401);
                        $token = $jwt->decodeJWT($authentication[1]);
                        $user = $this->entityManager->getRepository(Utilisateur::class)->findOneBy(["uid" => $token["sub"]]);
                    }
                }
            }
            elseif ($uid != "me" && $request->headers->has("Authentication")) {
                $authentication = explode(' ', $request->headers->get("Authentication"));
                if ("Bearer" != $authentication[0]) throw new \Exception("Il est nécessaire d'être authentifié", 401);
                else {
                    $jwt = $this->entityManager->getRepository(Token::class)->findOneBy(["accessToken" => $authentication[1]]);
                    if (empty($jwt)) throw new \UnexpectedValueException("Il est nécessaire d'être authentifié", 401);
                    $token = $jwt->decodeJWT($authentication[1]);
                    $useredit = $this->entityManager->getRepository(Utilisateur::class)->findOneBy(["uid" => $token["sub"]]);
                    if (!in_array("ROLE_ADMIN", $useredit->getRoles())) throw new \Exception("Il est nécessaire d'être admin ou d'être le proriétaire du compte", 403);
                    else{
                        unset($token, $jwt, $useredit);
                        $user = $this->entityManager->getRepository(Utilisateur::class)->findOneBy(['uid' => $uid]);
                    }
                }            
            }
            else $user = $this->entityManager->getRepository(Utilisateur::class)->findOneBy(['uid' => $uid]);
            

            if (!$user) return $this->json(['erreur' => 'Aucun utilisateur trouvé avec l\'UID donné'], 404);


            $data = json_decode($request->getContent(), true);
            
            
            
            $newPassword = $data['password'];
            $newLogin = $data["login"];
            $newRoles = $data["roles"];
            $newStatus = $data["status"];

            if ($newPassword) $user->setPassword($this->entityManager->getRepository(Token::class)->passwordHashed($newPassword));
            else throw new \Exception("Paramètres de connection invalide: mot de passe ou login manquant et / ou incorrect", 422);
            if ($newLogin) $user->setLogin($newLogin);
            else throw new \Exception("Paramètres de connection invalide: mot de passe ou login manquant et / ou incorrect", 422);

            if(in_array("ROLE_ADMIN", $user->getRoles()))if ($newRoles) $user->setRoles($newRoles);
            if ($newStatus) $user->setStatus($newStatus);


            $user->setUpdatedAt(new DateTimeImmutable());
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $userArray = $this->serializer->serialize($user, "json", ['attributes' => ['id', 'uid', 'login', 'roles', 'status', 'createAt', 'updatedAt']]);
            return $this->json(json_decode($userArray), 201);
        } catch (\Exception $e) {
            return $this->json([$e->getMessage()], $e->getCode());
        }
    }

    public function hash(String $password): string{
        return $this->passwordEncoder->hashPassword((new Utilisateur()), $password);
    }

}
