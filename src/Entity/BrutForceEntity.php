<?php

namespace App\Entity;

use App\Repository\BrutForceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BrutForceEntityRepository::class)]
class BrutForceEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $ip = null;

    #[ORM\Column(length: 255)]
    private ?string $fakepassword = null;

    #[ORM\Column(length: 255)]
    private ?string $fakelogin = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $delais = null;

    #[ORM\Column]
    private ?int $retry = null;

    #[ORM\Column]
    private ?bool $blockIp = null;

    #[ORM\Column]
    private ?int $etat = null;

    #[ORM\Column(length: 255)]
    private ?string $initdatetime = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): static
    {
        $this->ip = $ip;

        return $this;
    }

    public function getFakepassword(): ?string
    {
        return $this->fakepassword;
    }

    public function setFakepassword(string $fakepassword): static
    {
        $this->fakepassword = $fakepassword;

        return $this;
    }

    public function getFakelogin(): ?string
    {
        return $this->fakelogin;
    }

    public function setFakelogin(string $fakelogin): static
    {
        $this->fakelogin = $fakelogin;

        return $this;
    }

    public function getDelais(): ?\DateTimeInterface
    {
        return $this->delais;
    }
    
    public function setDelaisEchet(string $delais): static
    {
        $date = new \DateTime($delais);
        $this->delais = $date->add(new \DateInterval("PT5M"));

        return $this;
    }


    public function setDelaisPause(String $delais): static
    {
        $date = new \DateTime($delais);
        $this->delais = $date->add(new \DateInterval("PT30M"));

        return $this;
    }


    public function getRetry(): ?int
    {
        return $this->retry;
    }

    public function Retry(): static
    {
        $this->retry++;

        return $this;
    }

    public function isBlockIp(): ?bool
    {
        return $this->blockIp;
    }

    public function setBlockIp(): static
    {
        $this->blockIp = true;

        return $this;
    }

    public function setDeblockIp(): static {
        $this->blockIp = false;
        return $this;
    }

    public function getEtat(): ?int
    {
        return $this->etat;
    }

    public function setEcheque(): static
    {
        $this->etat = 0;

        return $this;
    }

    public function setPause(): static {
        $this->etat = 1;
        return $this;
    }


    public function getInitdatetime(): ?string
    {
        return $this->initdatetime;
    }

    public function setInitdatetime(string $initdatetime): static
    {
        $this->initdatetime = $initdatetime;

        return $this;
    }
}
