<?php

namespace App\Repository;

use App\Entity\BrutForceEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BrutForceEntity>
 *
 * @method BrutForceEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method BrutForceEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method BrutForceEntity[]    findAll()
 * @method BrutForceEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BrutForceEntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BrutForceEntity::class);
    }

    public function brutForche(String $login, String $password, mixed $ip) {
        $entitys = $this->findBy(["ip" => $ip[0]]);
        if (!empty($entitys) || $entitys != null) {
            foreach ($entitys as $e) {
                if($e->getFakelogin() == $login && $e->getFakepassword() == $password) {
                    $brutFroceEntity = $e;
                    break;
                }
            }
        }
        else {
            $brutFroceEntity = new BrutForceEntity();
            $brutFroceEntity->setFakelogin($login);
            $brutFroceEntity->setFakepassword($password);
            $brutFroceEntity->setIp($ip[0]);
            $brutFroceEntity->setDeblockIp();
            $brutFroceEntity->setInitdatetime((new \DateTime())->format("Y-m-d H:i:s"));  
            $brutFroceEntity->setDelaisEchet((new \DateTime())->format("Y-m-d H:i:s"));  
        }
        if ((new \DateTime() >= $brutFroceEntity->getInitdatetime() && new \DateTime() <= $brutFroceEntity->getDelais()) && !$brutFroceEntity->isBlockIp()) {
            if ($brutFroceEntity->getRetry() >= 3) {
                $brutFroceEntity->setDelaisPause((new \DateTime())->format("Y-m-d H:i:s"));
                $brutFroceEntity->setBlockIp();
                $brutFroceEntity->setPause();
                $this->getEntityManager()->persist($brutFroceEntity);
                $this->getEntityManager()->flush();
                throw new \Exception("Retry: Il faut attendre 30 min pour retester", 404);
            }
            else {
                $brutFroceEntity->Retry();
                $brutFroceEntity->setEcheque();
                $this->getEntityManager()->persist($brutFroceEntity);
                $this->getEntityManager()->flush();
                throw new \Exception("Retry", 404);
            }
        }
        elseif ($brutFroceEntity->getEtat() == 1 && (new \DateTime() > $brutFroceEntity->getDelais())) {
            // etat == 0 -> l'ip n'est pas bloqué
            // etat == 1 -> l'ip est bloqué
            if ($brutFroceEntity->getDelais()->format("Y-m-d") >= (new \DateTime())->format("Y-m-d")) {
                $this->getEntityManager()->remove($brutFroceEntity);
                $this->getEntityManager()->flush();
                throw new \Exception("", 200);
            }
        }
        else throw new \Exception("Retry: Il faut attendre 30 min pour retester", 404);
    }



    //    /**
    //     * @return BrutForceEntity[] Returns an array of BrutForceEntity objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('b.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?BrutForceEntity
    //    {
    //        return $this->createQueryBuilder('b')
    //            ->andWhere('b.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
